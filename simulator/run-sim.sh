#!/bin/sh
if [ -d sim-vis.ms ]; then
	echo Removing product
	rm -rf sim-vis.ms
fi	

echo Building simulation using parameters in generate_inputs.py and tools in Yandsoft image
IMAGE_NAME=sim_image:temp
docker build --no-cache -t ${IMAGE_NAME} -f build-sim.docker .
echo Creating image so we can get the artifact out
docker container create --name extract ${IMAGE_NAME}
docker container cp extract:/home/simulation/sim-vis.ms ./sim-vis.ms
docker rm -f extract
if [ -d sim-vis.ms ]; then
	echo Success
	exit 0
else
	echo No product built	
	exit 1
fi

