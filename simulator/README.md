CBF-SDP Simulation
==================

This is a Docker application that pulls the YandaSoft simulation and generates a number of simulations using the AA0.5 antenna layout

Requirements
----------------

Docker and a good internet connection as we are going to pull quite large images. Also some of the generated files will be quite large.

Configure 
-----------

Configuration is done via the LOW-AA0.5_test.in file. It contains all the station positions - This is based on the SKA-TEL-SKO-0000422_03_SKA1_LowConfigurationCoordinates.txt. 

There is a generate_inputs.py script written by Daniel Mitchell for YAN-139 that is included as part of the configuration. In this file you can change the number of channels, centre frequency, channel width and integration time.

Running
---------

Simply execute ./run-sim.sh - this will create the image containing the resultant simulation - and create a container to copy the image out of the container. The measurement set will be created in the directory which you run the script.

Notes:

The output visibility set is called sim_vis.ms
The initial run may take a while as the yandasoft image will need to be pulled.


