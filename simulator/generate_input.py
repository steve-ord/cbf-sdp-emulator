#!/usr/bin/python

NANTENNA = 4
NCHANNELS = 4
CENTRE_FREQ_MHz = 150.
CHAN_WIDTH_MHz = 0.05
INTEGRATION_TIME = 0.9

from numpy import *

# -------------------------------------------------------------------------------------------------------------------- #
# generate smoothly varying gains for each of Ns stations and across Nf frequency channels
#  - using FFTs, which leads to periodic gain spectra
def generate_csimulator_gains(Ns,Nf):

  gXre = zeros((Ns,Nf))
  gXim = zeros((Ns,Nf))
  gYre = zeros((Ns,Nf))
  gYim = zeros((Ns,Nf))

  # generate some low-order Fourier coefficients with which to generate smooth gain variations
  coeffs = zeros((Ns,Nf),"complex")
  coeffs[:,0] = 1.0
  coeffs[:,1] = 0.05*(random.randn(Ns) + 1j*random.randn(Ns))
  coeffs[:,2] = 0.02*(random.randn(Ns) + 1j*random.randn(Ns))
  gX = fft.fft(coeffs)

  coeffs = zeros((Ns,Nf),"complex")
  coeffs[:,0] = 1.0
  coeffs[:,1] = 0.05*(random.randn(Ns) + 1j*random.randn(Ns))
  coeffs[:,2] = 0.02*(random.randn(Ns) + 1j*random.randn(Ns))
  gY = fft.fft(coeffs)

  for ch in range(0,Nf):

    fout = open("gains_N%03d_ch%03d.in" % (Ns,ch), 'w')

    for stn in range(0,Ns):
        fout.write("gain.g11.%d.0 = [%f,%f]\n" % (stn, real(gX[stn,ch]), imag(gX[stn,ch])))

    for stn in range(0,Ns):
        fout.write("gain.g22.%d.0 = [%f,%f]\n" % (stn, real(gY[stn,ch]), imag(gY[stn,ch])))

    fout.close()

# -------------------------------------------------------------------------------------------------------------------- #
# generate input parset file for Csimulator
def generate_csimulator_params(Ns,Nf,f0,df):

  dt = INTEGRATION_TIME # sec

  for ch in range(0,Nf):

    f = f0 + df * (-(Nf/2) + ch)

    # RMS of vis noise (Braun, R. (2013). Understanding Synthesis Imaging Dynamic Range. A&A, 551:A91)
    wavelength = 299792458.0 / (f*1e6)
    T_sys = 150. + 60. * wavelength**(2.55)
    A_eff = 2. * 256. * wavelength**(2./3.)
    K = A_eff / (2. * 1.38e-23);
    rms = T_sys/K / sqrt(2.*(df*1e6)*dt) * 1e26;

    fout = open("csim_N%03d_ch%03d.in" % (Ns,ch), 'w')

    fout.write("Csimulator.dataset                              = vis_N%03d_ch%03d.ms\n" % (Ns,ch))
    fout.write("#\n")
    fout.write("# Define the sky model, either components or images, as well as visibility noise and station gains\n")
    fout.write("Csimulator.sources.names                        = [field1]\n")
    fout.write("Csimulator.sources.field1.direction             = [00h00m00.000, -26.74deg, J2000]\n")
    fout.write("Csimulator.sources.field1.model                 = image.gleamegc.skymodel.small\n")
    fout.write("Csimulator.sources.field1.nterms                = 2\n")

    fout.write("#\n")
    fout.write("# Add visibility noise\n")
    fout.write("#  - wavelength = %f m\n" % wavelength)
    fout.write("#  - T_sys = %f K\n" % T_sys)
    fout.write("#  - A_eff = %f m^2\n" % A_eff)
    fout.write("#  - rms = %f Jy\n" % rms)
    fout.write("Csimulator.noise                                = true\n")
    fout.write("Csimulator.noise.rms                            = %f\n" % rms)
    fout.write("#\n")
    fout.write("# Add gain corruptions\n")
    fout.write("Csimulator.corrupt                              = true\n")
    fout.write("Csimulator.corrupt.leakage                      = true\n")
    fout.write("Csimulator.calibaccess                          = parset\n")
    fout.write("Csimulator.calibaccess.parset                   = gains_N%03d_ch%03d.in\n" % (Ns,ch))
    fout.write("#\n")
    fout.write("# Define the antenna locations, feed locations, and spectral windows\n")
    fout.write("Csimulator.antennas.definition                  = LOW_AA0.5_test.in\n")
    fout.write("Csimulator.feeds.names                          = [feed0]\n")
    fout.write("Csimulator.feeds.feed0                          = [0.0, 0.0]\n")
    fout.write("#\n")
    fout.write("Csimulator.spws.names                           = [Single]\n")
    fout.write("Csimulator.spws.Single                          = [1, %fMHz, %fMHz, \"XX XY YX YY\"]\n" % (f,df))
    fout.write("#\n")
    fout.write("# Standard settings for the simulaton step\n")
    fout.write("Csimulator.simulation.blockage                  = 0.01\n")
    fout.write("Csimulator.simulation.elevationlimit            = 8deg\n")
    fout.write("Csimulator.simulation.autocorrwt                = 1.0\n")
    fout.write("Csimulator.simulation.usehourangles             = True\n")
    fout.write("Csimulator.simulation.referencetime             = [2015June24, UTC]\n")
    fout.write("#\n")
    fout.write("# Set other observation parameters\n")
    fout.write("Csimulator.simulation.integrationtime           = %fs\n" % dt)
    fout.write("Csimulator.observe.number                       = 1\n")
    fout.write("Csimulator.observe.scan0                        = [field1, Single, -0h01m, 0h01m]\n")
    fout.write("#\n")
    fout.write("# Use a (de)gridder to apply the primary beam and/or w terms (if using an image-based sky model).\n")
    fout.write("Csimulator.gridder                              = AWProject\n")
    fout.write("Csimulator.gridder.AWProject.nwplanes           = 21\n")
    fout.write("Csimulator.gridder.AWProject.wmax               = 6\n")
    fout.write("Csimulator.gridder.AWProject.wstats             = true\n")
    fout.write("Csimulator.gridder.AWProject.oversample         = 8\n")
    fout.write("Csimulator.gridder.AWProject.maxsupport         = 1024\n")
    fout.write("Csimulator.gridder.AWProject.diameter           = 35\n")
    fout.write("Csimulator.gridder.AWProject.blockage           = 0\n")
    fout.write("Csimulator.gridder.AWProject.variablesupport    = true\n")
    fout.write("Csimulator.gridder.AWProject.offsetsupport      = true\n")
    fout.write("Csimulator.gridder.AWProject.cutoff             = 1e-4\n")
    fout.write("Csimulator.gridder.AWProject.illumination       = SKA_LOW\n")
    fout.write("Csimulator.gridder.AWProject.illumination.pointing.ra  = 00h00m00.000\n")
    fout.write("Csimulator.gridder.AWProject.illumination.pointing.dec = -26.74deg\n")

    fout.close()

# -------------------------------------------------------------------------------------------------------------------- #
# generate input parset file for Ccalibrator
def generate_ccalibrator_params(Ns,Nf):

  fout = open("ccal_N%03d.in" % Ns, 'w')

  fout.write("Ccalibrator.dataset                              = vis_N%03d.ms\n" % Ns)
  fout.write("#\n")
  fout.write("#Ccalibrator.chanperworker                       = 1\n")
  fout.write("#Ccalibrator.chunk                               = %w\n")
  fout.write("#\n")
  fout.write("# Define the sky model, either components or images, as well as visibility noise and station gains\n")
  fout.write("Ccalibrator.sources.names                        = [field1]\n")
  fout.write("Ccalibrator.sources.field1.direction             = [00h00m00.000, -26.74deg, J2000]\n")
  fout.write("Ccalibrator.sources.field1.model                 = image.gleamegc.skymodel.small\n")
  fout.write("Ccalibrator.sources.field1.nterms                = 2\n")
  fout.write("#\n")
  fout.write("Ccalibrator.calibaccess                          = parset\n")
  fout.write("Ccalibrator.calibaccess.parset                   = bandpass_N%03d.out\n" % Ns)
  fout.write("#\n")
  fout.write("Ccalibrator.solve                                = bandpass\n")
  fout.write("Ccalibrator.solver                               = LSQR\n")
  fout.write("Ccalibrator.ncycles                              = 25\n")
  fout.write("#\n")
  fout.write("Ccalibrator.nAnt                                 = %d\n" % Ns)
  fout.write("Ccalibrator.nChan                                = %d\n" % Nf)
  fout.write("#\n")
  fout.write("# Use a (de)gridder to apply the primary beam and/or w terms (if using an image-based sky model).\n")
  fout.write("#  - using component-based sky model, so just specify a simple gridder (a gridder is needed)\n")
  fout.write("Ccalibrator.gridder                              = AWProject\n")
  fout.write("Ccalibrator.gridder.AWProject.nwplanes           = 21\n")
  fout.write("Ccalibrator.gridder.AWProject.wmax               = 6\n")
  fout.write("Ccalibrator.gridder.AWProject.oversample         = 8\n")
  fout.write("Ccalibrator.gridder.AWProject.maxsupport         = 1024\n")
  fout.write("Ccalibrator.gridder.AWProject.diameter           = 35\n")
  fout.write("Ccalibrator.gridder.AWProject.blockage           = 0\n")
  fout.write("Ccalibrator.gridder.AWProject.variablesupport    = true\n")
  fout.write("Ccalibrator.gridder.AWProject.offsetsupport      = true\n")
  fout.write("Ccalibrator.gridder.AWProject.cutoff             = 1e-4\n")
  fout.write("Ccalibrator.gridder.AWProject.illumination       = SKA_LOW\n")
  fout.write("Ccalibrator.gridder.AWProject.illumination.pointing.ra  = 00h00m00.000\n")
  fout.write("Ccalibrator.gridder.AWProject.illumination.pointing.dec = -26.74deg\n")
  fout.write("#\n")
  fout.write("#===================================================================\n")
  fout.write("\n")
  fout.write("# make this smaller if running on multiple workers\n")
  fout.write("Ccalibrator.chanperworker                        = %d\n" % Nf)
  fout.write("Ccalibrator.chunk                                = %w\n")
  fout.write("\n")
  fout.write("Ccalibrator.solver.LSQR.parallelMatrix           = true\n")
  fout.write("Ccalibrator.solver.LSQR.smoothing                = false\n")

  fout.close()

# -------------------------------------------------------------------------------------------------------------------- #
# generate shell script to run Csimulator and merge the results
def generate_run_scripts(Ns,Nf):

  Nbl = (Ns*(Ns-1))/2

  fout = open("run.sh", 'w')

  fout.write("#!/bin/bash\n")

  for ch in range(0,Nf):

    name = "csim_N%03d_ch%03d" % (Ns,ch)
    fout.write("csimulator -c %s.in > %s.log\n" % (name,name))

  name = "vis_N%03d" % Ns
  fout.write("msmerge -x 4 -c 1 -r %d -o sim-vis.ms %s_ch???.ms\n" % (Nf*Nbl,name))
  fout.write("#msmerge -x 4 -c %d -r %d -o tmp1.ms %s_ch?[0-3]?.ms\n" % (Nf,Nbl,name))
  fout.write("#msmerge -x 4 -c %d -r %d -o tmp2.ms %s_ch?[4-7]?.ms\n" % (Nf,Nbl,name))
  fout.write("#msmerge -x 4 -c %d -r %d -o sim-vis.ms tmp1.ms tmp2.ms\n" % (Nf,Nbl))

  fout.close()

# -------------------------------------------------------------------------------------------------------------------- #
# main

# number of stations
Ns = NANTENNA 

# number of frequencies
Nf = NCHANNELS
# central frequency and channel width (in MHz)
f0 = CENTRE_FREQ_MHz
df = CHAN_WIDTH_MHz

# rm -f csim*.in ccal*.in gains*.in run*.sh

generate_csimulator_gains(Ns,Nf)
generate_csimulator_params(Ns,Nf,f0,df)
generate_ccalibrator_params(Ns,Nf)
generate_run_scripts(Ns,Nf)

