# -*- coding: utf-8 -*-
"""Reads data off a MS and creates SPEAD2 packets"""

import argparse
import configparser
import contextlib
import logging
import time

import numpy
from oskar import measurement_set
import spead2.send

from cbf_sdp import transmitters
import sys

from cbf_sdp import payloads

logger = logging.getLogger(__name__)
DEFAULT_CONFIG_FILE = 'packetiser.conf'



def baselines(stations):
    return (stations * (stations + 1)) // 2


def channel_setup(ms, config):
    start_chan = int(config.get('start_chan', 0))
    if start_chan > ms.num_channels:
        raise ValueError(
            f'start_chan = {start_chan} > num_chan in MS ({ms.num_channels})')
    num_chan = int(config.get('num_chan', ms.num_channels))
    end_chan = start_chan + num_chan
    if end_chan > ms.num_channels:
        num_chan = ms.num_channels - start_chan
        logger.warning(
            'start_chan + num_chan = %d > num_chan in MS (%d), reducing num_chan to %d',
            end_chan, ms.num_channels, num_chan
        )
    return start_chan, num_chan


def vis_reader(ms, config):

    # Antennas
    num_stations = ms.num_stations
    num_baselines = baselines(num_stations)

    # Channels
    start_chan, num_chan = channel_setup(ms, config)

    # Timestamps
    timestamps_in_ms = ms.num_rows // num_baselines
    num_timestamps = int(config.get('num_timestamps', 0))
    if num_timestamps < 0:
        raise ValueError(f'num_timestamps must be >= 0: {num_timestamps}')
    if num_timestamps == 0:
        num_timestamps = timestamps_in_ms
    elif num_timestamps > timestamps_in_ms:
        logger.warning('%d > num_timestamps in MS (%d), reducing to %d',
                       num_timestamps, timestamps_in_ms, timestamps_in_ms)
        num_timestamps = timestamps_in_ms

    # Read until we exhaust the MS
    start_row = 0
    timestamp_count = 0
    while start_row < ms.num_rows and timestamp_count < num_timestamps:
        yield ms.read_vis(start_row, start_chan, num_chan, num_baselines)
        start_row += num_baselines
        timestamp_count += 1


def packetise(config, ms):
    """Packets data in a MeasurementSet within SPEAD-64-48 heaps"""

    ms = measurement_set.MeasurementSet.open(ms)
    num_stations = ms.num_stations
    num_baselines = baselines(num_stations)
    start_chan, num_chan = channel_setup(ms, config['reader'])
    logger.info(f'no. stations      : {num_stations}')
    logger.info(f'no. baselines     : {num_baselines}')
    logger.info(f'no. channels      : {num_chan}')
    logger.info(f'first channel     : {start_chan}')

    myPayload = payloads.create(config['payload'],num_baselines,num_chan)
    item_group = myPayload.item_group

    # Repeats
    num_repeats = int(config['reader'].get('num_repeats', 1))
    if num_repeats <= 0:
        raise ValueError(f'num_repeats must be > 0: {num_repeats}')

    # Iterate over timesteps in the data
    start_time = time.time()
    nbytes = 0
    with contextlib.closing(
        transmitters.create(config['transmission'], item_group)
    ) as transmitter:
        for vis_amps in vis_reader(ms, config['reader']):
            item_group['correlator_output_data'].value['VIS'] = vis_amps
            heap = item_group.get_heap(descriptors='all', data='all')
            repeat_count = 0
            while repeat_count < num_repeats:
                repeat_count += 1
                nbytes += vis_amps.nbytes
                transmitter.send(heap)

    # Print time taken.
    duration = time.time() - start_time
    data_size = transmitter.num_streams * nbytes / 1024 / 1024
    logger.info(
        "Sent %.3f MB in %.3f sec (%.3f MB/sec)",
        data_size,
        duration,
        (data_size / duration),
    )


def _config_parser(f):
    config_parser = configparser.ConfigParser()
    config_parser.read(f)
    if 'transmission' not in config_parser:
        config_parser['transmission'] = {}
    if 'reader' not in config_parser:
        config_parser['reader'] = {}
    if 'payload' not in config_parser:
        config_parser['payload'] = {}
    return config_parser


def _augment_config(config, options):
    for opt in options:
        name, value = opt.split('=')
        category, name = name.split('.')
        if category not in config:
            config[category] = {}
        config[category][name] = value

def main():

    parser = argparse.ArgumentParser(
        description="Creates SPEAD2 heaps out of a MS file"
    )
    parser.add_argument(
        "-c",
        '--config',
        help="The configuration file to load, defaults to %s" % DEFAULT_CONFIG_FILE,
        default=DEFAULT_CONFIG_FILE,
        type=_config_parser,
    )
    parser.add_argument(
        "-o",
        "--option",
        help="Additional configuration options in the form of category.name=value",
        action='append'
    )
    parser.add_argument('measurement_set', help="The measurement set to read data from")

    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    config = args.config
    _augment_config(config, args.option)
    packetise(config, args.measurement_set)

if __name__ == '__main__':
    main()
