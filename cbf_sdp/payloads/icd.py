# -*- coding: utf-8 -*-
import logging


logger = logging.getLogger(__name__)

CORR_OUT_TYPE = [('TCI', 'i1'), ('FD', 'u1'), ('VIS', '<c8', 4)]

import numpy
import spead2.send


#"""This defines a SPEAD2 payload consistent with the CBF-SDP ICD as of April 2020 """

class payload(object):
    """SPEAD2 payload following the CSP-SDP interface document"""

    def __init__(self,config,num_baselines, num_channels):
        
        self.item_group = spead2.send.ItemGroup(flavour=spead2.Flavour(4, 64, 48, 0))
        self.item_group.add_item(
                id=0x6000,
                name='visibility_timestamp_count',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6001,
                name='visibility_timestamp_fraction',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6002,
                name='visibility_channel_id',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6003,
                name='visibility_channel_count',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6004,
                name='visibility_polarisation_id',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6005,
                name='visibility_baseline_count',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u4',
                )
        self.item_group.add_item(
                id=0x6008,
                name='scan_id',
                description='',
                shape=tuple(),
                format=None,
                dtype='<u8',
                )
        self.item_group.add_item(
                id=0x600A,
                name='correlator_output_data',
                description='',
                shape=(num_channels, num_baselines,),
                dtype=CORR_OUT_TYPE,
                )
        vis = numpy.zeros(shape=(num_channels, num_baselines,), dtype=CORR_OUT_TYPE)
        self.item_group['visibility_timestamp_count'].value = 1
        self.item_group['visibility_timestamp_fraction'].value = 0
        self.item_group['visibility_baseline_count'].value = num_baselines
        self.item_group['visibility_channel_id'].value = 12345
        self.item_group['visibility_channel_count'].value = num_channels
        self.item_group['visibility_polarisation_id'].value = 0
        self.item_group['scan_id'].value = 100000000
        self.item_group['correlator_output_data'].value = vis

    def example(self):
        """Example: Define non return function for subsequent test."""

    def example_2(self):
        """Example: Define function for subsequent test with specific return value."""
        return 2
