# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = '0.0.0'

__author__ = 'Stephen Ord'
__email__ = 'stephen.ord@csiro.au'

def create(config, num_baselines, num_chan):
    """ return instance of payload determined by the config """
    import importlib
    method = config.get('method', 'icd').lower()
    m = importlib.import_module("." + method, package=__name__)
    return getattr(m, 'payload')(config, num_baselines, num_chan)
