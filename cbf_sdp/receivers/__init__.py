# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = '0.0.0'

__author__ = 'Stephen Ord'
__email__ = 'stephen.ord@csiro.au'

def create(config, item_group):
    import importlib #provides import fuctionality
    method = config.get('method', 'spead2_receivers').lower() # get the method from the config
    m = importlib.import_module("." + method, package=__name__) # import the method from the package
    return getattr(m, 'receiver')(config, item_group) # getattr returns m.transmitter(args)
