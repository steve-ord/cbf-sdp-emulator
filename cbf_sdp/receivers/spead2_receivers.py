# -*- coding: utf-8 -*-

from __future__ import print_function, division
import logging
import spead2
import spead2.recv
import sys


DEFAULT_CONFIG_FILE = 'receiver.conf'

logger = logging.getLogger(__name__)

#"""Define function placeholders and test function examples."""


# TODO: Replace all the following code with the desired functionality for the package
def receivers_function_example():
    """Example: function outside of a class"""


# TODO: Replace all the following code with the desired functionality for the package
class receiver:
    """Define class, methods etc"""

    def __init__(self, config, item_group):
        
        
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
        logger.info(f'Starting single stream receiver') 
        #loop = asyncio.get_event_loop() # asyncio event loop
        #assert loop # keep the linter happy for the moment


        # set up a single stream and attach the threadpool to it.
        stream = spead2.recv.Stream(spead2.ThreadPool(threads=1))
        pool = spead2.MemoryPool(16384, 26214400, 12, 8) # memory allocation for the input
        stream.set_memory_allocator(pool) # set the memory for the stream
        
        # can I add more than one reader for a stream?
        port = int(config.get('receiver_port_start', 8888))
        logger.info(f'Starting single stream receiver on port %d',port) 
        stream.add_udp_reader(port) 

        self.num_heaps = 0
        for heap in stream:
            logger.debug('Got heap %d', heap.cnt)
            item_group.update(heap)
            self.num_heaps += 1
        stream.stop()
        logger.info(f'Received {self.num_heaps} heaps') 
   

    def example(self):
        """Example: Define non return function for subsequent test."""

    def example_2(self):
        """Example: Define function for subsequent test with specific return value."""
        return 2


