# -*- coding: utf-8 -*-
"""Network transmission methods"""
import logging
import math


logger = logging.getLogger(__name__)

import spead2
import spead2.send as spead2_send


class transmitter(object):

    def __init__(self, config, item_group):
        self.config = config
        self.item_group = item_group
        max_packet_size = int(config.get('max_packet_size', 1472))
        logger.info(
            'Creating StreamConfig with max_packet_size=%d',
            max_packet_size)
        self.stream_config = spead2.send.StreamConfig(
            max_packet_size=max_packet_size,
            rate=int(config.get('rate', 1024 * 1024 * 1024)),
            burst_size=10,
            max_heaps=1,
        )
        self.channels_per_streams = int(
            config.get('channels_per_stream', 0))
        self.sender_threads = int(
            config.get('sender_threads', 1))
        self.num_streams = 0  # set on first call to send()
        self.streams = []
        self._create_streams(
            item_group['correlator_output_data'].value.shape[0])

    def _create_streams(self, num_channels):
        if self.channels_per_streams == 0:
            self.num_streams = 1
        else:
            self.num_streams = math.ceil(num_channels / self.channels_per_streams)
        logger.info(
            'Creating %d spead2 streams to send data for %d channels',
            self.num_streams,
            num_channels)

        thread_pool = spead2.ThreadPool(threads=self.sender_threads)
        config = self.config
        target_host = config.get('target_host', '127.0.0.1')
        target_port = int(config.get('target_port_start', 41000))
        for i in range(self.num_streams):
            port = target_port + i
            logger.info("Sending to %s:%d", target_host, port)
            stream = spead2.send.UdpStream(
                thread_pool=thread_pool,
                hostname=target_host,
                port=port,
                config=self.stream_config,
            )
            stream.send_heap(self.item_group.get_start())
            self.streams.append(stream)

    def send(self, heap):
        for stream in self.streams:
            stream.send_heap(heap)

    def close(self):
        # Send end-of-stream message.
        for stream in self.streams:
            stream.send_heap(self.item_group.get_end())
