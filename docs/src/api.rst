.. doctest-skip-all
.. _package-guide:


API documentation
=================

This section describes requirements and guidelines.

.. automodule:: cbf_sdp.packetiser
    :members:

.. automodule:: cbf_sdp.transmitters.spead2_transmitters
    :members:

.. automodule:: cbf_sdp.payloads.icd
    :members:
