Installation
============

Dependencies
------------

Optionally, first create a virtual environment.

This package uses the OSKAR MS libraries
for doing Measurement Set reading and writing.
The OSKAR project contains numerous other functionality though
which can be more expensive to build and install.
To install **only** this part of the C++ library
follow these instructions::

  # Install dependencies and build tool
  apt-get -y update
  apt-get -y install cmake libblas-dev liblapack-dev casacore-dev

  # Get a copy of OSKAR
  git clone https://github.com/OxfordSKA/OSKAR.git
  mkdir OSKAR/oskar/ms/release
  cd OSKAR/oskar/ms/release

  # Add -DCMAKE_INSTALL_PREFIX=$VIRTUAL_ENV if installing into a virtual
  # environment
  cmake -DCASACORE_LIB_DIR=/usr/lib/x86_64-linux-gnu ..

  # Build and install
  make -j4
  make install

  # Got back to the root directory where you started
  cd ../../../..

Our package does not use the C++ OSKAR MS library directly
but its python bindings.
However, the OSKAR python bindings package
does not support out of the box
to install **only** the MS part of the bindings.
Thus, the following steps will be required::

  # Get a copy of this package
  git clone https://gitlab.com/ska-telescope/cbf-sdp-emulator

  # Use this package's copy of setup.py to build OSKAR's python bindings
  cd OSKAR/python
  mv ../../cbf-sdp-simulator/3rdParty/setup_oskar_ms.py ./setup.py
  python3 ./setup.py build
  python3 ./setup.py install
  cd ../../

This package
------------

This is a standard setuptools-based program
so the usual installation methods should work::

  # Go into the top-level directory of this repository
  cd cbf-sdp-emulator

  # Using "pip" will automatically install any dependencies from PyPI
  pip install .

  # Use pip in editable mode if you are actively changing the code
  pip install -e .
