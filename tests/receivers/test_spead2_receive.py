#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import pytest
import threading
import spead2
import spead2.send
import spead2
import logging
import sys
import configparser
 
from cbf_sdp.receivers import spead2_receivers
from cbf_sdp import packetiser

logger = logging.getLogger(__name__)


lock = threading.Lock()

RECEIVER_STATS='/tmp/receiver_stats.txt'
                
class receiveThread (threading.Thread):
    
    def __init__(self, threadID,config,itemgroup):
      
        threading.Thread.__init__(self,daemon=True)
        self.threadID = threadID
        self.config = config
        self.itemgroup = itemgroup
        self.num_heaps = 0
      
        
    def run(self):
        lock.acquire()
        
        rcvr = spead2_receivers.receiver(self.config, self.itemgroup) 
        # write the number of heaps received (-1 for the end)
        with open(RECEIVER_STATS,'w') as results:
            results.write(str(rcvr.num_heaps) + '\n')
            results.flush()
        
        lock.release()
            
        
@pytest.fixture(autouse=True,scope="module")
def example_Receiver():
    
    
    # set the results file to 0
    with open(RECEIVER_STATS,'w') as results:
            results.write('0')
            results.flush()
            
    config = configparser.ConfigParser()
    config['reception'] = {}
    config['payload'] = {}
    config['reception']['method'] = 'spead2_receivers'
    config['reception']['receiver_port_start'] = '41001'
    config['payload']['method'] = 'icd' 
    
    ig = spead2.send.ItemGroup()
    
    Receive = receiveThread(2,config['reception'],ig)
    Receive.start()
    yield Receive

def test_single_stream():
    """Example: Assert the spead2 receiver code."""

   
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
   
    config = configparser.ConfigParser()

    config['reception'] = {}
    config['reception']['method'] = 'spead2_receivers'
    config['transmission'] = {}
    config['transmission']['method'] = 'spead2_transmitters'
    config['transmission']['target_host'] = '127.0.0.1'
    config['transmission']['target_port_start'] = '41001'
    config['transmission']['channels_per_stream'] = '4'
    config['payload'] = {}
    config['payload']['method'] = 'icd' 
    
    
    config['reader'] = {}
    

  
   
    import time
    time.sleep(2) #just arbitrary to get the socket up
    logger.info("starting the transmitter")
    try:
        import os.path
        logger.info(
            f'current directory %s',os.getcwd())
        if os.path.isdir("tests/receivers/sim-vis.ms"):
            logger.info(
            'Input file exists')
        else:
            raise(IOError)
           
    except IOError:
        logger.error(
                f'File not accessible')
        raise
    
        
    packetiser.packetise(config,"tests/receivers/sim-vis.ms")
    
    # the number of HEAPS in this test should be 134 
    lock.acquire(True)
    with open(RECEIVER_STATS,'r') as f:
        r = f.read()
        if (int(r) != 134):
            pytest.fail(f'Expected 134 HEAPS, got {int(r)}')
    lock.release()        
