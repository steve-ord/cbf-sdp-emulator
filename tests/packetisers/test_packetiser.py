#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import pytest

from cbf_sdp import packetiser


# TODO: Replace all the following examples with tests for the ska_python_skeleton package code
def test_something():
    """Example: Assert with no defined return value."""
    assert True


def test_with_error():
    """Example: Assert raising error."""
    with pytest.raises(ValueError):
        # Do something that raises a ValueError
        raise ValueError

def test_baselines():
    assert packetiser.baselines(4) == 10
    
def test_package():   
    import configparser
    config = configparser.ConfigParser()
    
    
    config['reader'] = {'start_chan' : 0}
    config['reader'] = {'num_chan' : 16}
    config['transmission'] = {'method' : 'spead2_transmitters'}
    config['transmission'] = {'target_host' : '127.0.0.1'}
    config['transmission'] = {'target_port_start' : 41000}
    config['transmission'] = {'channels_per_stream' : 4}
    config['payload'] = { 'method' : 'icd' }
    
    packetiser.packetise(config,"tests/data/sim-vis.ms")
    
    
# Fixture example
@pytest.fixture
def an_object():
    """Example: Define fixture for subsequent test."""
    return {}


def test_consumers_skeleton(an_object):
    """Example: Assert ure return value."""
    assert an_object == {}



