#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import pytest

from cbf_sdp import transmitters
from cbf_sdp import payloads


# 



def test_something():
    """Example: Assert with no defined return value."""
    assert True


def test_with_error():
    """Example: Assert raising error."""
    with pytest.raises(ValueError):
        # Do something that raises a ValueError
        raise ValueError


# Fixture example
@pytest.fixture
def an_object():
    """Example: Define fixture for subsequent test."""
    return {}


def test_transmitters_skeleton(an_object):
    """Example: Assert fixture return value."""
    assert an_object == {}


def test_single_stream():
    """Example: Assert the spead2 transmitter code."""

    import configparser
    config = configparser.ConfigParser()
    config['transmission'] = {}
    config['payload'] = {}
    config['transmission']['method'] = 'spead2_transmitters'
    config['transmission']['target_host'] = '127.0.0.1'
    config['transmission']['target_port_start'] = '41000'
    config['payload']['method'] = 'icd' 
    
    num_baselines = 10
    num_channels = 16
    
    foo = payloads.create(config['payload'],num_baselines,num_channels)
    heap = foo.item_group.get_heap(descriptors='all', data='all')
    bar = transmitters.create(config['transmission'],foo.item_group)
    bar.send(heap)
    bar.close()
    
def test_multiple_streams():
    """Test multiple stream generation"""
    import configparser
    config = configparser.ConfigParser()
    config['transmission'] = {}
    config['payload'] = {}
    config['transmission']['method'] = 'spead2_transmitters'
    config['transmission']['target_host'] = '127.0.0.1'
    config['transmission']['target_port_start'] = '41000'
    config['transmission']['channels_per_stream'] = '4'
    config['payload']['method'] = 'icd' 

    
    num_baselines = 10
    num_channels = 16
    
    foo = payloads.create(config['payload'],num_baselines,num_channels)
    heap = foo.item_group.get_heap(descriptors='all', data='all')
    bar = transmitters.create(config['transmission'],foo.item_group)
    bar.send(heap)
    bar.close()
    
