import pytest
import os
import sh

def test_simulator():
  try:
    sh.docker(['--version'])
  except sh.CommandNotFound as e:
    print(e) 
    pytest.skip("likely docker not installed or runnable ... maybe a CI problem") 
  except:
    pytest.fail("Problem with docker other than not being installed")

  cwd = os.getcwd()
  # change directories to the generated project directory 
  # (the installation command must be run from here)
  project_dir = cwd + "/simulator/" 
  os.chdir(str(project_dir))
  # run the shell command
  rtn_code = os.system("./run-sim.sh")

  # always change directories to the test directory
  os.chdir(cwd)
  assert(rtn_code == 0)  
